import React, { Component } from 'react'
import Job from './Job'
import { jobsList } from '../data/datasource';
// import Job components

class WorkHistory extends Component {
  render() {
    console.log('???');
    /* receive `jobsList` array as props from App compnonent */

    return (
      <section>
        <h4>Work Experience</h4>
        <div className="skills-list">
          {
            jobsList.map(function (job) {
              return <Job topic={job.title}
                wrkPlace={job.company}
                info={job.description}
                beggins={job.years[0]}
                ends={job.years[1]}

              />;

            })
          }
        </div>
      </section>
    )
  }
}

export default WorkHistory;
