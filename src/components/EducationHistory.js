import React, { Component } from 'react'
import EduTitle from './EduTitle';
import { eduList } from '../data/datasource';
// import EduTitle component

class EducationHistory extends Component {
  render() {

    /* receive `eduList` array as props from App compnonent
    aqui traigo todo el objeto en map */

    return (
      <section>
        <h4>Education</h4>
        <div className="skills-list">

          {eduList.map(function (school) {
            return <EduTitle insti={school.institute}
              kmpStudy={school.fieldOfStudy}
              fecha={school.dates}
            />;
          })


          }

        </div>
      </section>
    )
  }
}

export default EducationHistory;
