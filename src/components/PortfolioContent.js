import React, { Component } from "react";

import Header from "./Header"
import Summary from "./Summary"
import ContactInfo from "./ContactInfo"
import SkillsList from "./SkillsList";
import EducationHistory from "./EducationHistory";
import WorkHistory from "./WorkHistory";
import Job from "./Job";

class PortfolioContent extends Component {
  render() {
    return (
      <div className="portfolio-content">
        <Header />
        <Summary />
        <ContactInfo />
        <SkillsList />
        <EducationHistory />
        <WorkHistory />
      </div>
    )
  }
}

export default PortfolioContent
