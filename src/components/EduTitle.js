import React, { Component } from 'react'

class EduTitle extends Component {
  render() {

    return (
      <div className="degree">
        <h5 className="degree__institute">{this.props.insti} </h5>
        <p className="degree__field">{this.props.kmpStudy}</p>
        <p className="degree__dates">{this.props.fecha}</p>
      </div>
    );
  }
}

export default EduTitle
