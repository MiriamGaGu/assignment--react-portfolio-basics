
import React, { Component } from 'react'

class Job extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="job">
        <div className="job__years">
          <h6 className="job__end">{this.props.ends}</h6>
          <h6 className="job__start">{this.props.beggins}</h6>
        </div>
        <h5 className="job__title">{this.props.topic}</h5>
        <h5 className="job__company">{this.props.wrkPlace}</h5>
        <p className="job__description">{this.props.info}</p>
      </div>
    );
  }
}

export default Job
