import React, { Component } from 'react'
import Skill from './Skill'
import { skills } from '../data/datasource';
// import Skill component

class SkillsList extends Component {
  render() {
    return (
      /* receive `skills` array as props from App compnonent */

      < section >
        <h4>Skills</h4>
        <div className="skills-list">

          {
            skills.map(function (skills) {
              return < Skill coolSkills={skills} />
            })}
        </div>
      </section >
    );
  }
}

export default SkillsList;

